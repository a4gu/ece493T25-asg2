
import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q1 = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.q2 = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name = "Double QLearning"

    def choose_Q(self, q_table, observation):
        self.check_state_exist(observation)

        if np.random.uniform() >= self.epsilon:
            state_action = q_table.loc[observation, :]
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
            action = np.random.choice(self.actions)
        return action

    def choose_action(self, observation):
        self.check_state_exist(observation)

        if np.random.uniform() >= self.epsilon:
            state_action = self.q1.loc[observation, :]  # only q1
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
            action = np.random.choice(self.actions)
        return action

    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        if s_ != 'terminal':
            p = np.random.random()
            if (p < .5):
                a_ = self.choose_Q(self.q1, str(s_))
                self.q1.loc[s, a] = self.q1.loc[s, a] + \
                    self.lr * (r + self.gamma * np.max(self.q2.loc[s_, :]) - self.q1.loc[s, a])
            else:
                a_ = self.choose_Q(self.q2, str(s_))
                self.q2.loc[s, a] = self.q2.loc[s, a] + \
                    self.lr * (r + self.gamma * np.max(self.q1.loc[s_, :]) - self.q2.loc[s, a])
        else:
            # self.q1.loc[s, a] = r  # next state is terminal
            maxq = np.max(self.q2.loc[s_, :])
            self.q1.loc[s, a] = self.q1.loc[s, a] + self.lr * (r + self.gamma * maxq - self.q1.loc[s, a])  # next state is terminal
            self.q2.loc[s, a] = self.q2.loc[s, a] + self.lr * (r + self.gamma * maxq - self.q2.loc[s, a])  # next state is terminal
        return s_, a_

    def check_state_exist(self, state):
        if state not in self.q1.index:
            # append new state to q table
            self.q1 = self.q1.append(
                pd.Series(
                    [0] * len(self.actions),
                    index=self.q1.columns,
                    name=state,
                )
            )

        if state not in self.q2.index:
            # append new state to q table
            self.q2 = self.q2.append(
                pd.Series(
                    [0] * len(self.actions),
                    index=self.q2.columns,
                    name=state,
                )
            )
